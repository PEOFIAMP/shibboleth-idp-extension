# はじめに

これはShibboleth IdPからSPに送られる属性情報を、DH, Cascade, Agent方式の
いずれかの方式に則って暗号化する拡張ソフトです。
Shibboleth IdP 2.3.8 と協調動作します。

本拡張のコンパイルにはApache Maven (http://maven.apache.org/)を用います。
コンパイル環境にApache Mavenがあることを確認してください。

コンパイルにはShibboleth IdPの環境にインストールされているものと同じ
JDKを用いることを推奨します。検証時にはJDK6 (6u27)を用いました。

Shibboleth IdPのインストールされている環境とコンパイルする環境が異なる場合、
Shibboleth IdPの環境にApache Mavenを用意する必要はありません。

# コンパイル方法

コンパイルするには、mvnコマンドを以下のように実行します

> mvn -DskipTests clean install

注意: 初回インストール時 Apache Maven がコンパイルに必要なコンポーネントを
自動的にダウンロードします。

コンパイルの結果、Shibboleth IdPにおいて利用できるjarファイル
(gakunin-attribute-definitions-0.1.jar)が、
プロジェクトの target/ ディレクトリ配下に作成されます。

# インストール方法

Shibboleth IdPのインストール用パッケージに、上記でコンパイルしたプラグインを含め、パッケージインストールを行います。

本マニュアルでは学認技術ガイド(https://www.gakunin.jp/technical/)の「インストール（貴学にてIdPをインストールする場合の構築手順）」に沿ってインストールされたShibboleth IdPを仮定して内容を記述します。
OSや環境変数については、ご自身の環境に併せて読み替えて設定を行なってください。

既に動作しているIdPに対して作業を行う場合には、まず起動しているtomcat6とhttpdを停止します


(学認の提供する起動スクリプトを利用している場合)
# service tomcat6 stop
# service httpd stop


"service tomcat6 stop" は、学認 技術ガイドにおいて提供されている
tomcat6の自動起動スクリプトを用いた場合にのみ行えます。
利用していない場合には、代わりに以下のコマンドを実行してください


(学認の提供する起動スクリプトを利用しない場合)
# sh /usr/java/tomcat/bin/shutdown.sh
# service httpd stop

tomcat6とhttpdを停止した状態で、コンパイルしたIdP拡張のjarファイルを
インストール用パッケージのlib/ディレクトリにコピーします。

> cp gakunin-extra-attribute-definitions-0.1.jar shibboleth-identity-provider-2.3.8/lib/

IdP拡張をコピーしたら、rootユーザになるか、もしくは sudo を用いて、
通常通りShibboleth IdPの更新を行います。

# ./install.sh
(ここで、既にShibboleth IdPがインストール済の場合、設定を上書きするかどうかを聞かれます)
# cp /opt/shibboleth-idp/war/idp.war ${CATALINA_HOME}/webapps


なお、仮に ${CATALINA_HOME}/webapps/ (CentOSの通常では /var/lib/tomcat6/webapps/)ディレクトリ配下に
idp/ ディレクトリが展開されている場合には、それを削除します。


# rm -rf /var/lib/tomcat6/webapps/idp/


# service httpd start
# service tomcat6 start (起動スクリプトを利用する場合)
# sh /usr/java/tomcat/bin/startup.sh (起動スクリプトを利用しない場合)


# 拡張機能の利用方法

本拡張を用いることで、Shibboleth IdPの attribute-resolver.xml に追加で以下の追加属性を設定することが出来るようになります。

 * urn:mace:gakunin.jp:idprivacy:ad:SecureSimple
  * AttributeDefinitionとして動作し、受け取ったデータを暗号化します
  * urn:mace:shibboleth:2.0:resolver:ad:Simpleに対応します
  * urn:mace:shibboleth:2.0:attribute:encoder:SAML2SecureStringと組み合わせて用います
 * urn:mace:gakunin.jp:idprivacy:ad:SecureScoped
  * AttributeDefinitionとして動作し、受け取ったデータを暗号化します
  * urn:mace:shibboleth:2.0:resolver:ad:Scopedに対応します
  * urn:mace:shibboleth:2.0:attribute:encoder:SAML2SecureScopedStringと組み合わせて用います
 * urn:mace:gakunin.jp:idprivacy:dc:DhParams
  * DH方式に関わる属性を生成するDataConnectorです
 * urn:mace:gakunin.jp:idprivacy:dc:AgentParams
  * Agent方式に関わる属性を生成するDataConnectorです

attribute-resolver.xml に設定を追加後、Proxyとなるホストに向けてそれらを開放するようにしてください (attribute-filter.xml を適切に設定します)。

