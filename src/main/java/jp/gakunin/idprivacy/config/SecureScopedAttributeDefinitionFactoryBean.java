/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.config;

import org.opensaml.xml.util.DatatypeHelper;

import jp.gakunin.idprivacy.attributeDefinition.SecureScopedAttributeDefinition;
import jp.gakunin.idprivacy.attributeDefinition.SecureSimpleAttributeDefinition;
import edu.internet2.middleware.shibboleth.common.config.attribute.resolver.attributeDefinition.BaseAttributeDefinitionFactoryBean;

public class SecureScopedAttributeDefinitionFactoryBean
        extends BaseAttributeDefinitionFactoryBean {
    
    private String mScope;

    public Class<?> getObjectType() {
        return SecureScopedAttributeDefinition.class;
    }

    public String getScope() {
        return mScope;
    }

    public void setScope(String newScope) {
        mScope = DatatypeHelper.safeTrimOrNullString(newScope);
    }

    protected Object createInstance() throws Exception {
        SecureScopedAttributeDefinition definition = new SecureScopedAttributeDefinition(mScope);
        populateAttributeDefinition(definition);

        return definition;
    }
}