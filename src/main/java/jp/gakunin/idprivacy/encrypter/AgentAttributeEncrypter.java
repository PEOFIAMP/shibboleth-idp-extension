/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.encrypter;

import java.io.ByteArrayInputStream;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import jp.gakunin.idprivacy.Definitions;

import org.opensaml.xml.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AgentAttributeEncrypter implements AttributeEncrypter {

    private final Logger log = LoggerFactory.getLogger(AgentAttributeEncrypter.class);
    
    private Cipher mCipher;
    private PublicKey mPublicKey;

    /**
     * Tries to instanciate an AgentEncrypter object with SAMLRequest DOM,
     * which should be available via ShibbolethResolutionContext.
     *
     * This assumes that a protocol type for this call is agent. No check
     * will be done here.
     *  
     * @param dom DOM object representing SAMLRequest.
     * @return an object if successful. null otherwise.
     */
    public static AgentAttributeEncrypter instanciateWithSamlRequest(Element dom) {
        AgentAttributeEncrypter encrypter = new AgentAttributeEncrypter();
        if (encrypter.init(dom)) {
            return encrypter;
        } else {
            return null;
        }
    }

    private AgentAttributeEncrypter() {
    }
    
    private boolean init(Element dom) {
        final NodeList pubKeyNodes = dom
                .getElementsByTagName(Definitions.INCOMING_ATTR_AGENT_PUB_KEY);
        if (pubKeyNodes.getLength() == 0) {
            log.error("No Agent Pub Key available.");
            return false;
        }

        // Note:
        // PEM format of RSA pub-key and X509 certificate is actually BASE64 encoded der format
        // with header and footer ("BEGIN" and "END" lines).
        // Here we assume input is intermediate: no header/footer, base64-encoded.

        // pem (base64 encoded der form)
        String pem = pubKeyNodes.item(0).getTextContent();
        byte[] der = Base64.decode(pem);
        if (der == null) {
            log.error("Failed to decode input pub key.");
            return false;
        }

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(der);
            mPublicKey = keyFactory.generatePublic(keySpec);
            mCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            mCipher.init(Cipher.ENCRYPT_MODE, mPublicKey);
            log.debug("PublicKey generated with KeyFactory.");
            
            return true;
        } catch (GeneralSecurityException e) {
            log.error("Exception when preparing public key with KeyFactor: " + e.getMessage());
        }
        
        // We still have hope.

        try {
            CertificateFactory certFactory = CertificateFactory.getInstance("X509");
            Certificate cert = certFactory.generateCertificate(new ByteArrayInputStream(der));
            mPublicKey = cert.getPublicKey();
            mCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            mCipher.init(Cipher.ENCRYPT_MODE, mPublicKey);
            
            /* Quick test to decrypt it. Note that, input private key must be un-encrypted
             * pkcs8 format, which is different from openssl default rsa key.
             *
             * Here's an example to transform a default key in pem format to pkcs8 der 
             * > openssl pkcs8 -topk8 -nocrypt -inform pem -in cert.key \
             * -outform der -out cert_pkcs8.der
             *
             * Then, you probably need to encode it with base64
             * > base64 cert_pkcs8.der
             */
            /*
            try {
            String test = "hoge_hoge-日本語";
            byte[] encoded = mCipher.doFinal(test.getBytes("UTF-8"));
            log.debug("encoded length:" + encoded.length);
            
            
            String priv = encryptedPrivKeyNodes.item(0).getTextContent();
            log.debug("@@@ priv=" + priv);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            KeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.decode(priv));
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
            Cipher decCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            decCipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decoded = decCipher.doFinal(encoded);
            log.debug("@@@ encoded-decoded: " + new String(decoded, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                log.error("Exception: " + e.getMessage());
                return false;
            }*/
            return true;
        } catch (GeneralSecurityException e) {
            log.error("Exception: " + e.getMessage());
            return false;
        }
        // Never reach here.
    }

    public byte[] encrypt(byte[] src) {
        try {
            return mCipher.doFinal(src);
        } catch (GeneralSecurityException e) {
            log.error("Exception: " + e.getMessage());
            return null;
        }
    }

    public String getProtocolType() {
        return Definitions.PROTOCOL_TYPE_AGENT;
    }
}