/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.encrypter;

/**
 * Encrypts given raw bytes using some schema.
 *
 * For DH/Cascade, the encryption will be achieved with
 * another byte array.
 *
 * For Agent, this will be a (RSA) private key prepared by
 * Agent (browser extension).
 */
public interface AttributeEncrypter {
    public byte[] encrypt(byte[] value);
    
    /** Returns protocol type (mainly for testing purpose) */
    public String getProtocolType();
}