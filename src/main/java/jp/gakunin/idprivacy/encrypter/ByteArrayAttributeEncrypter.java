/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.encrypter;

/**
 * 
 * TODO: DO NOT REUSE SECRET.
 */
public class ByteArrayAttributeEncrypter implements AttributeEncrypter {
    final byte[] mSecret;
    final String mProtocolType;

    private ByteArrayAttributeEncrypter(byte[] secret, String protocolType) {
        mSecret = secret;
        mProtocolType = protocolType;
    }

    public static ByteArrayAttributeEncrypter instanciate(byte[] secret, String protocolType) {
        if (secret != null) {
            // TODO: do we want to clone it?
            ByteArrayAttributeEncrypter encoder = new ByteArrayAttributeEncrypter(
                    secret, protocolType);
            return encoder;
        } else {
            return null;
        }
    }

    // TODO: Currently this implementation copies another byte array which might be
    // inefficient. Try modifying a given array instead.
    public byte[] encrypt(byte[] value) {
        byte[] encrypted = new byte[value.length];
        for (int i = 0; i < encrypted.length; i++) {
            // TODO: DO NOT REUSE SECRET. THIS WILL BREAK SECURITY.
            encrypted[i] = (byte) (value[i] ^ mSecret[i % mSecret.length]);
        }
        return encrypted;
    }

    public String getProtocolType() {
        return mProtocolType;
    }
}