/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;

import jp.gakunin.idprivacy.encrypter.AgentAttributeEncrypter;
import jp.gakunin.idprivacy.encrypter.AttributeEncrypter;
import jp.gakunin.idprivacy.encrypter.ByteArrayAttributeEncrypter;

import org.opensaml.xml.security.x509.X509Credential;
import org.slf4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.AttributeResolutionException;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.ShibbolethResolutionContext;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.dataConnector.DataConnector;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;

public final class Utils {
	private Utils() {
	}

	/**
	 * Returns an appropriate protocol type by inspecting a given DOM element.
	 *
	 * The string may not be any of 
	 * {@link Definitions#PROTOCOL_TYPE_DH}, {@link Definitions#PROTOCOL_TYPE_CASCADE}, or 
	 * {@link Definitions#PROTOCOL_TYPE_AGENT} when the original DOM contains
	 * some garbage(, which won't be usual though).
	 *
	 * @param dom
	 * @param log
	 * @return protocol type coming from DOM.
	 */
	public static String obtainProtocolTypeFromDom(Element dom, Logger log) {
        final NodeList protocolTypeNodes = dom
                .getElementsByTagName(Definitions.INCOMING_ATTR_PROTOCOL_TYPE);
        final String protocolType;
        if (protocolTypeNodes.getLength() == 0) {
            log.error("No protocol_type available.");
            protocolType = Definitions.PROTOCOL_TYPE_NONE;
        } else {
            if (protocolTypeNodes.getLength() > 1) {
                log.warn("More than one protocol_type available. Choosing the first one");
            }
            protocolType = protocolTypeNodes.item(0).getTextContent();
        }
        log.debug(String.format("Protocol type=\"%s\"", protocolType));
        return protocolType;
	}
	
    @SuppressWarnings("rawtypes")
    public static byte[] obtainDhSecret(ShibbolethResolutionContext context,
            List<String> dependencyIds, Logger log) {
        for (String id : dependencyIds) {
            DataConnector connector = context.getResolvedDataConnectors().get(id);
            if (connector != null) {
                try {
                    Map<String, BaseAttribute> attributes = connector.resolve(context);
                    if (attributes.containsKey(Definitions.GENERATED_ATTR_DH_SHARED_SECRET)) {
                        BaseAttribute attr = attributes
                                .get(Definitions.GENERATED_ATTR_DH_SHARED_SECRET);
                        if (attr.getValues().size() > 0) {
                            String hexSharedSecret = attr.getValues().iterator().next().toString();
                            log.debug("DH secret obtained from id \"" + id + "\"");
                            return Utils.fromHexString(hexSharedSecret);
                        }
                    }
                } catch (AttributeResolutionException e) {
                }
            }
        }
        return null;
    }

    /**
     * Obtains encrypted K2 from PxS and decrypt it with IdP's private key.

     */
    public static byte[] obtainCascadeSecret(
            ShibbolethResolutionContext context, Element dom, Logger log) {
        // K2 is coming from PxS, which should be used for encrypting
        // all the attributes.
        final NodeList encryptedK2Nodes = dom
                .getElementsByTagName(Definitions.INCOMING_ATTR_CASCADE_IDP_ENCRYPTED_K2);
        if (encryptedK2Nodes.getLength() == 0) {
            log.error("No K2 available.");
            return null;
        }
        if (encryptedK2Nodes.getLength() > 1) {
            log.warn("More than one K2 available. Choosing the first one.");
        }

        byte[] encryptedK2 = Utils.fromHexString(encryptedK2Nodes.item(0).getTextContent());

        // Retrieve IdP's own private key from its relying-party configuration
        // (relying-party.xml).
        final RelyingPartyConfiguration rpConfig = context.getAttributeRequestContext()
                .getRelyingPartyConfiguration();
        X509Credential credential = (X509Credential) rpConfig.getDefaultSigningCredential();
        try {
            final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, credential.getPrivateKey());
            return Utils.fromHexString(new String(cipher.doFinal(encryptedK2)));
        } catch (GeneralSecurityException e) {
            log.error("Exception during decrypting K2. " + e.toString());
        }

        return null;
    }

    public static String encode(String raw, AttributeEncrypter encrypter) {
        return Utils.toHexString(encrypter.encrypt(raw.getBytes(Charset.forName("utf-8"))));
    }

	public static String encode(String raw, byte[] secret) {
        byte[] rawBytes = raw.getBytes(Charset.forName("utf-8"));
        for (int i = 0; i < rawBytes.length; i++) {
            rawBytes[i] = (byte) (rawBytes[i] ^ secret[i % secret.length]);
        }

        String encoded = Utils.toHexString(rawBytes);
        return encoded;
    }
	
	public static String decode(String encoded, byte[] secret) {
	    return decode(encoded, secret, null);
	}

	public static String decode(String encoded, byte[] secret, Logger log) {
        byte[] encodedBytes = Utils.fromHexString(encoded);
        for (int i = 0; i < encodedBytes.length; i++) {
            encodedBytes[i] = (byte) (encodedBytes[i] ^ secret[i
                    % secret.length]);
        }

        String raw;
        try {
            raw = new String(encodedBytes, "utf-8");
        } catch (UnsupportedEncodingException e) {
            if (log != null) {
                log.error("utf-8 is not supported :-P ", e.getMessage());
            }
            raw = null;
        }

        return raw;
    }

	public static String toHexString(byte[] array) {
    	StringBuilder builder = new StringBuilder();
    	for (int i = 0; i < array.length; i++) {
    		String hex = Integer.toHexString(array[i] & 0xff);
    		if (hex.length() == 1) {
    			builder.append('0');
    		}
			builder.append(hex);
    	}
    	return builder.toString();
    }

	public static byte[] fromHexString(String hexString) {
        int size = hexString.length();
        byte[] array = new byte[size/2];
        for (int i = 0; i < array.length; i++) {
            int val = Character.digit(hexString.charAt(i*2), 16) << 4;
            val |= Character.digit(hexString.charAt(i*2 + 1), 16);
            array[i] = (byte) val;
        }
		return array;
	}

	private static AttributeEncrypter constructAttributeEncrypter(
	        ShibbolethResolutionContext context, List<String> dependencyIds, Logger log) {
        final Element dom = context.getAttributeRequestContext().getInboundMessage().getDOM();
        final String mProtocolType = Utils.obtainProtocolTypeFromDom(dom, log);
        
        if (Definitions.PROTOCOL_TYPE_DH.equals(mProtocolType)) {
            return ByteArrayAttributeEncrypter.instanciate(
                    Utils.obtainDhSecret(context, dependencyIds, log),
                    Definitions.PROTOCOL_TYPE_DH);
        } else if (Definitions.PROTOCOL_TYPE_CASCADE.equals(mProtocolType)) {
            return ByteArrayAttributeEncrypter.instanciate(
                    Utils.obtainCascadeSecret(context, dom, log),
                    Definitions.PROTOCOL_TYPE_CASCADE);
        } else if (Definitions.PROTOCOL_TYPE_AGENT.equals(mProtocolType)) {
            return AgentAttributeEncrypter.instanciateWithSamlRequest(dom);
        }

        return null;
	}

	public static boolean setupEncrypterIfPossible(SecureBasicAttribute<?> attribute,
	        ShibbolethResolutionContext context, List<String> dependencyIds, Logger log) {
        AttributeEncrypter encrypter = Utils.constructAttributeEncrypter(
                context, dependencyIds, log);
        if (encrypter != null) {
            attribute.setEncrypter(encrypter);
            return true;
        } else {
            return false;
        }
	}
}