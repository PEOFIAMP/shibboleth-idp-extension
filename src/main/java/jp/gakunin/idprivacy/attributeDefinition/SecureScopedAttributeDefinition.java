/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.attributeDefinition;

import java.util.Collection;

import jp.gakunin.idprivacy.SecureBasicAttribute;
import jp.gakunin.idprivacy.Utils;

import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.provider.ScopedAttributeValue;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.AttributeResolutionException;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.ShibbolethResolutionContext;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.attributeDefinition.BaseAttributeDefinition;

public class SecureScopedAttributeDefinition extends BaseAttributeDefinition {

    private final Logger log = LoggerFactory.getLogger(SecureScopedAttributeDefinition.class);

    /** Scope value. */
    private final String mScope;

    /**
     * Constructor.
     * 
     * @param newScope scope of the attribute
     */
    public SecureScopedAttributeDefinition(String newScope) {
        this.mScope = newScope;
    }

    /** {@inheritDoc} */
    public BaseAttribute<ScopedAttributeValue> doResolve(
            ShibbolethResolutionContext resolutionContext)
            throws AttributeResolutionException {
        SecureBasicAttribute<ScopedAttributeValue> attribute =
                new SecureBasicAttribute<ScopedAttributeValue>();
        attribute.setId(getId());
        
        if (!Utils.setupEncrypterIfPossible(
                attribute, resolutionContext, getDependencyIds(), log)) {
            log.error("Encrypter isn't ready.");    
        }

        Collection<?> values = getValuesFromAllDependencies(resolutionContext);
        if (values != null && !values.isEmpty()) {
            for (Object value : values) {
                if (value != null) {
                    String strValue = DatatypeHelper.safeTrimOrNullString(value.toString());
                    if (strValue != null) {
                        attribute.getValues().add(new ScopedAttributeValue(
                                strValue.toString(), mScope));
                    }
                }
            }
        }

        return attribute;
    }

    /**
     * Get scope value.
     * 
     * @return Returns the scope.
     */
    public String getScope() {
        return mScope;
    }

    /** {@inheritDoc} */
    public void validate() throws AttributeResolutionException {
        // do nothing
    }
}