/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.attributeDefinition;

import java.util.Collection;

import jp.gakunin.idprivacy.SecureBasicAttribute;
import jp.gakunin.idprivacy.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.AttributeResolutionException;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.ShibbolethResolutionContext;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.attributeDefinition.BaseAttributeDefinition;

/*
 * Note:
 * This class intentionally inherits BaseAttributeDefinition instead of SimpleAttributeDefinition
 * and copy some of logic from the similar implementation.
 *
 * Ideally we should just inherit SimpleAttributeDefinition and reuse BaseAttribute object from it.
 * One problem is that BaseAttribute is not an interface but an abstract class, and thus inserting
 * "secret" in it without breaking its contract is a little difficult and dangerous (We need to
 * prepare SecureBaseAttribute or something and copy data in the original object to the new one,
 * but because the "old" one is actually abstract class which has its own implementation,
 * we cannot be fully sure if SecureBaseAttribute is able to copy all the detail of the old
 * instance).
 *
 * Here, this just copies the original StringAttributeDefinition class and forget all the detail
 * discussed above. Cross fingers and hope. 
 */
public class SecureSimpleAttributeDefinition extends BaseAttributeDefinition {

    private final Logger log = LoggerFactory.getLogger(SecureSimpleAttributeDefinition.class);

    public BaseAttribute<?> doResolve(ShibbolethResolutionContext resolutionContext) {
        final SecureBasicAttribute<Object> attribute = new SecureBasicAttribute<Object>();
        attribute.setId(getId());

        if (!Utils.setupEncrypterIfPossible(
                attribute, resolutionContext, getDependencyIds(), log)) {
            log.error("Encrypter isn't ready.");    
        }

        final Collection<?> values = getValuesFromAllDependencies(resolutionContext);
        attribute.getValues().addAll(values);
        return attribute;
    }

    public void validate() throws AttributeResolutionException {
    }
}