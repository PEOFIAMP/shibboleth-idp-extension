/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.dataConnector;

import java.util.HashMap;
import java.util.Map;

import jp.gakunin.idprivacy.Definitions;
import jp.gakunin.idprivacy.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.provider.BasicAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.AttributeResolutionException;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.ShibbolethResolutionContext;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.dataConnector.BaseDataConnector;

/**
 * DataConnector that retrieves necessary keys for Agent protocol from AuthnRequest's Extensions
 * field.
 */
public class AgentParamsDataConnector extends BaseDataConnector {

    private final Logger log = LoggerFactory.getLogger(AgentParamsDataConnector.class);

    @SuppressWarnings("rawtypes")
    public Map<String, BaseAttribute> resolve(ShibbolethResolutionContext resolutionContext)
            throws AttributeResolutionException {
        final Map<String, BaseAttribute> result = new HashMap<String, BaseAttribute>();
        final Element dom = resolutionContext.getAttributeRequestContext().getInboundMessage()
                .getDOM();
        String protocolType = Utils.obtainProtocolTypeFromDom(dom, log);
        if (!Definitions.PROTOCOL_TYPE_AGENT.equals(protocolType)) {
            return result;
        }
        log.debug(String.format("resolve(). id=\"%s\"", getId()));
        
        final String pubKey = getFirstTextContentByTagName(dom,
                Definitions.INCOMING_ATTR_AGENT_PUB_KEY);
        final String encryptedPrivKey =
                getFirstTextContentByTagName(dom,
                        Definitions.INCOMING_ATTR_AGENT_ENCRYPTED_PRIV_KEY);
        putNewAttribute(result, Definitions.GENERATED_ATTR_AGENT_PUB_KEY, pubKey);
        putNewAttribute(result, Definitions.GENERATED_ATTR_AGENT_ENCRYPTED_PRIV_KEY,
                encryptedPrivKey);
        
        // privKey and privKeyPkcs8 is just for debugging. Px and Sp will just receieve
        // encryptedPrivKey (and only SP can decrypt/use it).
        //
        // For debugging SP we'll use privKey (simple pem format, or pkcs7?).
        // Inside Java (IdP), we may utilize PKCS8 (Java support it with PKCS8EncodedKeySpec).  
        final String privKey =
                getFirstTextContentByTagName(dom,
                        Definitions.INCOMING_ATTR_AGENT_PRIV_KEY);
        final String privKeyPkcs8 =
                getFirstTextContentByTagName(dom,
                        Definitions.INCOMING_ATTR_AGENT_PRIV_KEY_PKCS8);
        putNewAttribute(result, Definitions.GENERATED_ATTR_AGENT_PRIV_KEY, privKey);
        putNewAttribute(result, Definitions.GENERATED_ATTR_AGENT_PRIV_KEY_PKCS8, privKeyPkcs8);
        return result;
    }

    public void validate() throws AttributeResolutionException {
    }

    // TODO: duplicate of DhParamsDataConnector's method.
    private String getFirstTextContentByTagName(Element dom, String tagName) {
        final NodeList nodes = dom.getElementsByTagName(tagName);
        if (nodes.getLength() == 0) {
            log.error(String.format("Agent param \"%s\" not available.", tagName));
            return null;
        } else if (nodes.getLength() > 1) {
            log.warn(String.format("Too many Agent params for tag \"%s\".", tagName));
        }
        return nodes.item(0).getTextContent();
    }
    
    // TODO: ditto
    @SuppressWarnings("rawtypes")
    private void putNewAttribute(Map<String, BaseAttribute> result, String key, String value) {
        BasicAttribute<Object> attribute = new BasicAttribute<Object>();
        attribute.getValues().add(value);
        result.put(key, attribute);
    }
}
