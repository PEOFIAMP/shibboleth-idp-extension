/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.dataConnector;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHKey;
import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHPublicKeySpec;

import jp.gakunin.idprivacy.Definitions;
import jp.gakunin.idprivacy.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.provider.BasicAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.AttributeResolutionException;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.ShibbolethResolutionContext;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.dataConnector.BaseDataConnector;

/**
 * DataConnector class for generating Diffie-Hellman key exchange protocol.
 * This generates the following values and allows attribute-resolver.xml to
 * use them as AttributeDefinitions. 
 *
 * <ul>
 * <li>P ({@link Definitions#GENERATED_ATTR_DH_P})</li>
 * <li>G ({@link Definitions#GENERATED_ATTR_DH_G})</li>
 * <li>Sender's public key ({@link Definitions#GENERATED_ATTR_DH_SP_PUB_KEY})</li>
 * <li>Sender's private key encrypted by SP's own key
 * ({@link Definitions#GENERATED_ATTR_DH_SP_ENCRYPTED_PRIV_KEY})</li>
 * <li>Resultant Shared secret ({@link Definitions#GENERATED_ATTR_DH_SHARED_SECRET})</li>
 * </ul>
 *
 * It is required that P, G, sender's pub key and sender's private key are embedded in
 * AuthnRequest from (DH method's) Proxy SP (a direct peer for this Shibboleth IdP).
 *
 */
public class DhParamsDataConnector extends BaseDataConnector {

    private final Logger log = LoggerFactory.getLogger(DhParamsDataConnector.class);

    private final String mDataType;

    private String mP;
    private String mG;
    private String mSenderY;
    private String mSenderEncryptedPrivKey;

    private PublicKey mSenderPublic;
    private PrivateKey mReceiverPrivate;
    private PublicKey mReceiverPublic;
    
    private byte[] mSharedSecret;

    public DhParamsDataConnector() {
        super();
        mDataType = null;
    }

    public DhParamsDataConnector(String dataType) {
        mDataType = dataType;
    }

    @SuppressWarnings("rawtypes")
    public Map<String, BaseAttribute> resolve(ShibbolethResolutionContext resolutionContext)
            throws AttributeResolutionException {
        final Map<String, BaseAttribute> result = new HashMap<String, BaseAttribute>();
        final Element dom = resolutionContext.getAttributeRequestContext().getInboundMessage()
                .getDOM();
        String protocolType = Utils.obtainProtocolTypeFromDom(dom, log);
        if (!Definitions.PROTOCOL_TYPE_DH.equals(protocolType)) {
            return result;
        }
        log.debug(String.format("resolve(). id=\"%s\"", getId()));

        if (!initDhParams(dom)) {
            log.error("Failed to initialize DH params. No resultant attributes will be generated.");
            return result;
        }

        final String receiverY = ((DHPublicKey) mReceiverPublic).getY().toString(16);
        final String hexSharedSecret = Utils.toHexString(mSharedSecret); 

        putNewAttribute(result, Definitions.GENERATED_ATTR_DH_P, mP);
        putNewAttribute(result, Definitions.GENERATED_ATTR_DH_G, mG);
        putNewAttribute(result, Definitions.GENERATED_ATTR_DH_IDP_PUB_KEY, receiverY);
        putNewAttribute(result, Definitions.GENERATED_ATTR_DH_SP_PUB_KEY, mSenderY);
        putNewAttribute(result, Definitions.GENERATED_ATTR_DH_SP_ENCRYPTED_PRIV_KEY,
                mSenderEncryptedPrivKey);
        putNewAttribute(result, Definitions.GENERATED_ATTR_DH_SHARED_SECRET, hexSharedSecret);
        return result;
    }

    private boolean initDhParams(Element dom) {
        mP = getFirstTextContentByTagName(dom, Definitions.INCOMING_ATTR_DH_P);
        mG = getFirstTextContentByTagName(dom, Definitions.INCOMING_ATTR_DH_G);
        mSenderY = getFirstTextContentByTagName(dom, Definitions.INCOMING_ATTR_DH_SP_PUB_KEY);
        mSenderEncryptedPrivKey = getFirstTextContentByTagName(dom,
                Definitions.INCOMING_ATTR_DH_SP_ENCRYPTED_PRIV_KEY);
        if (mP == null || mG == null || mSenderY == null || mSenderEncryptedPrivKey == null) {
            return false;
        }
        return initDhParams(new BigInteger(mP, 16), new BigInteger(mG, 16),
                new BigInteger(mSenderY, 16));
    }

    private boolean initDhParams(BigInteger p, BigInteger g, BigInteger y) {
        try {
            final KeyFactory keyFactory = KeyFactory.getInstance("DH");
            final DHPublicKeySpec senderKeySpec = new DHPublicKeySpec(y, p, g);
            mSenderPublic = keyFactory.generatePublic(senderKeySpec);
            KeyPairGenerator receiverKeyPairGen = KeyPairGenerator.getInstance("DH");
            receiverKeyPairGen.initialize(((DHKey) mSenderPublic).getParams());

            KeyPair receiverKeyPair = receiverKeyPairGen.generateKeyPair();
            mReceiverPrivate = receiverKeyPair.getPrivate();
            mReceiverPublic = receiverKeyPair.getPublic();
            KeyAgreement receiverAgreement = KeyAgreement.getInstance("DH");
            receiverAgreement.init(mReceiverPrivate);

            receiverAgreement.doPhase(mSenderPublic, true);

            // Resultant DH secret.
            mSharedSecret = receiverAgreement.generateSecret();

            log.debug("p=" + mP);
            log.debug("g=" + mG);
            log.debug("sp_pub=" + mSenderY);
            log.debug("idp_priv=" + ((DHPrivateKey) mReceiverPrivate).getX().toString(16));
            log.debug("idp_pub=" + ((DHPublicKey) mReceiverPublic).getY().toString(16));
            log.debug("sp_e_priv_key=" + mSenderEncryptedPrivKey);
            log.debug("shared_secret=" + Utils.toHexString(mSharedSecret));
            return true;
        } catch (GeneralSecurityException e) {
            log.error("GeneralSecurityException: " + e.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
            return false;
        }
    }

    private String getFirstTextContentByTagName(Element dom, String tagName) {
        final NodeList nodes = dom.getElementsByTagName(tagName);
        if (nodes.getLength() == 0) {
            log.error(String.format("DH param \"%s\" not available.", tagName));
            return null;
        } else if (nodes.getLength() > 1) {
            log.warn(String.format("Too many DH params for tag \"%s\".", tagName));
        }
        return nodes.item(0).getTextContent();
    }

    @SuppressWarnings("rawtypes")
    private void putNewAttribute(Map<String, BaseAttribute> result, String key, String value) {
        BasicAttribute<Object> attribute = new BasicAttribute<Object>();
        attribute.getValues().add(value);
        result.put(key, attribute);
    }

    public void validate() throws AttributeResolutionException {
    }

    
    
    public String getDataType() {
        return mDataType;
    }
}