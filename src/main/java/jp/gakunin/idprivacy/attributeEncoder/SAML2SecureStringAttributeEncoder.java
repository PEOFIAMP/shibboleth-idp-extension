/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gakunin.idprivacy.attributeEncoder;

import java.util.List;

import jp.gakunin.idprivacy.EncrypterHolder;
import jp.gakunin.idprivacy.Utils;
import jp.gakunin.idprivacy.encrypter.AttributeEncrypter;

import org.opensaml.Configuration;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.schema.impl.XSStringBuilder;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.encoding.AttributeEncodingException;
import edu.internet2.middleware.shibboleth.common.attribute.encoding.provider.AbstractSAML2AttributeEncoder;

public class SAML2SecureStringAttributeEncoder extends AbstractSAML2AttributeEncoder {

    private final Logger log = LoggerFactory.getLogger(SAML2SecureStringAttributeEncoder.class);

    private final XMLObjectBuilder<XSString> mStringBuilder = (XSStringBuilder)
            Configuration.getBuilderFactory().getBuilder(XSString.TYPE_NAME);

    public Attribute encode(BaseAttribute attribute) throws AttributeEncodingException {
        final AttributeEncrypter encrypter;
        if (attribute instanceof EncrypterHolder) {
            EncrypterHolder holder = (EncrypterHolder) attribute;
            encrypter = holder.getEncrypter();
        } else {
            log.error("Failed to obtain encrypter from attribute. "
                    + "Maybe resolver's setting has problems. "
                    + "Object class of \"" + attribute.getId() + "\": "
                    + attribute.getClass().getCanonicalName());
            encrypter = null;
        }

        Attribute samlAttribute = attributeBuilder.buildObject();
        populateAttribute(samlAttribute);

        for (Object o : attribute.getValues()) {
            if (o == null) {
                continue;
            }
            String attributeValue = o.toString();
            if (!(DatatypeHelper.isEmpty(attributeValue))) {
                if (encrypter != null) {
                    // value -> hex-encoded encrypted value. 
                    attributeValue = Utils.encode(attributeValue, encrypter);
                }

                XSString samlAttributeValue = mStringBuilder.buildObject(
                        AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
                samlAttributeValue.setValue(attributeValue);
                samlAttribute.getAttributeValues().add(samlAttributeValue);
            }
        }

        List<XMLObject> attributeValues = samlAttribute.getAttributeValues();
        if (attributeValues == null || attributeValues.isEmpty()) {
            log.debug("Unable to encode {} attribute.  It does not contain any values",
                    attribute.getId());
            return null;
        }

        return samlAttribute;
    }
}