/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.gakunin.idprivacy;

import jp.gakunin.idprivacy.attributeDefinition.SecureSimpleAttributeDefinition;
import jp.gakunin.idprivacy.attributeEncoder.SAML2SecureStringAttributeEncoder;
import jp.gakunin.idprivacy.encrypter.AttributeEncrypter;
import edu.internet2.middleware.shibboleth.common.attribute.provider.BasicAttribute;

/**
 * BasicAttribute used with AttributeDefinitions like {@link SecureSimpleAttributeDefinition}
 * and AttributeEncoders like {@link SAML2SecureStringAttributeEncoder}.
 *
 * AttributeDefinitions store Encrypter in this object and pass it to AttributeEncoders.
 * Then the encoders encrypt attributes using the given Encrypter.
 */
public class SecureBasicAttribute<TypeName> extends BasicAttribute<TypeName>
        implements EncrypterHolder {
    private AttributeEncrypter mEncrypter;

    public void setEncrypter(AttributeEncrypter encrypter) {
        mEncrypter = encrypter;
    }

    public AttributeEncrypter getEncrypter() {
        return mEncrypter;
    }
}